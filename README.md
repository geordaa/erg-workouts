# ERG Workouts

ERG Formatted workouts for Trainer Road etc.

<table align="center"><tr><td align="center" width="9999">
<img src="img/dave.jpg" align="center" width="200" alt="Project icon">

# ERG-Workouts

A project to collect my publicly available workouts created for Trainer Road users and anyone looking for ERG format workouts.
</td></tr></table>

All my public workouts are dynamic based on the rider's own unique FTP value. Please conduct a FTP or Ramp Test and set your FTP value in your product of choice to gain the maximum advantage. 

## Workouts

<table align="center">
<tr><th>Workout Name</th><th>Duration</th><th>Training Stress</th><th>Visualisation</th></tr>
<tr><td>Climbing Repeats</td><td>1:05:00</td><td>79</td><td><img src="img/ClimbingReps.png" align="center" width="400" alt="Climbing Repeats"></td></tr>
<tr><td>Endurance Threshold</td><td>1:23:00</td><td>96</td><td><img src="img/EnduranceThreshold.png" align="center" width="400" alt="Endurance Threshold"></td></tr>
<tr><td>Power Adaptations</td><td>1:13:00</td><td>135</td><td><img src="img/ERGPowerAdaptations.png" align="center" width="400" alt="Power Adaptations"></td></tr>
<tr><td>Russian Steps</td><td>1:11:00</td><td>91</td><td><img src="img/RussianSteps.png" align="center" width="400" alt="Russian Steps"></td></tr>
</table>


## Good luck!
## Dave

